#!/bin/sh

jcl=1.3.4
log4j=2.24.1
jackson=2.18.1
mvnrepos=/D/m2repos

cp=target/classes
cp=${cp}:${mvnrepos}/commons-logging/commons-logging/${jcl}/commons-logging-${jcl}.jar
cp=${cp}:${mvnrepos}/org/apache/logging/log4j/log4j-api/${log4j}/log4j-api-${log4j}.jar
cp=${cp}:${mvnrepos}/org/apache/logging/log4j/log4j-core/${log4j}/log4j-core-${log4j}.jar
cp=${cp}:${mvnrepos}/org/apache/logging/log4j/log4j-jcl/${log4j}/log4j-jcl-${log4j}.jar
cp=${cp}:${mvnrepos}/com/fasterxml/jackson/core/jackson-core/${jackson}/jackson-core-${jackson}.jar
cp=${cp}:${mvnrepos}/com/fasterxml/jackson/core/jackson-annotations/${jackson}/jackson-annotations-${jackson}.jar
cp=${cp}:${mvnrepos}/com/fasterxml/jackson/core/jackson-databind/${jackson}/jackson-databind-${jackson}.jar
echo ${cp}

java -cp ${cp} -Dlog4j.configurationFile=log4j2.xml com.xlongwei.logserver.Log4j2Client