## logserver

##### 项目简介
使用logback和light-4j构建的简单日志服务，参考项目[logbackserver](https://gitee.com/xlongwei/logbackserver)和[light4j](https://gitee.com/xlongwei/light4j)。
logserver的设计初衷是聚合日志，解决登录多个linux主机查找日志的痛点。日志量大时使用grep方式可能有性能瓶颈，因此可选使用[light-search](https://gitee.com/lightgrp/light-search)创建索引和搜索。
建议使用完整的特征值（例如id值）进行搜索，而非频繁出现的部分字符串（例如搜索INFO遇到超大日志就会卡死），这时直接登录linux使用less查找更合适一些（日志已经聚合也不太麻烦）。

##### 本地测试

1. 项目构建：mvn package dependency:copy-dependencies -DoutputDirectory=target
2. 运行服务：start.bat，打开首页[index](http://localhost:9880/index.html)跟踪日志
3. client测试：client.bat，输入测试内容，浏览器会输出最新日志；sh client.sh 1.5，实测支持logback更多版本
4. [light-search](https://gitee.com/lightgrp/light-search)索引服务：-Dlight-search=http://localhost:9200 -DuseIndexer=true -DuseSearch=true
5. logger页签：可以动态变更应用日志级别，-Dlogger=name@url静态配置其他应用，[logserver-spring-boot-starter](https://gitee.com/xlongwei/logserver-spring-boot-starter)支持自动注册，变更时需要密码
6. [trace](https://gitee.com/xlongwei/logserver/wikis/trace)跟踪日志：提供请求头[X-Traceability-Id](http://115.28.229.158/tool/images/logserver/search.png)即可，后端通过[MyCorrelationHandler](https://gitee.com/xlongwei/light4j/blob/master/src/main/resources/config/handler.yml)写入MDC，并会在跨应用请求时传递此请求头
7. sift分开存储：-Dsift，根据contextName分开存储日志，不参与搜索和索引，因此-Dlogfile依然需要，如果在root去掉ASYNC_FILE，则需要修改sift内的notify=true，以便向浏览器输出日志。
8. my.pwd保存私密信息，-Dlogback.configurationFile=/home/logback.xml可以指定外部日志配置，编辑此文件即可变更日志级别。
9. logjannino.xml拆分为logfile.xml（收集日志写入文件logfile）、logserver.xml（收集日志转发到logserver），配合filebeat可以后台收集或转发日志，注释掉filebeat才会监听9880端口。

##### 线上部署

1. 项目打包：sh start.sh deploy，打包为单独的fat-jar；sh start.sh package，打包并复制依赖；start运行，status状态，rebuild重建，stop停止
2. -Dfiletailers=app1.log;app2.log，直接合并其他项目日志；ln -s /path/app1.log logs/app1.log，支持page分页查看其他日志
3. 其他项目的日志配置参考client.xml，或者参考[light4j](https://gitee.com/xlongwei/light4j/blob/master/src/main/resources/logback.xml)，通过/etc/hosts指定logserver
4. -Dlogfile=logs/all.logs 日志路径，logserver自身日志输出到Console，其他client应用日志输出到logfile
5. -Djava.compiler=none，禁用JIT可节约内存，默认启用JIT可提高性能
6. 定时压缩日志（logserver不再搜索），56 23 * * * sh /soft/shells/[tgz_logs.sh](https://gitee.com/xlongwei/logserver/blob/master/aliyun/tgz_logs.sh) >> /var/log/mycron_clears.log，
7. filebeat模式：vi start.sh，打开filebeat注释，用于跟踪多个日志文件，并发送日志内容到logserver，也支持redis媒介
8. 可选redis媒介：打开-Dredis注释，支持pubsub发布订阅和pushpop消息队列，详见[wiki](https://gitee.com/xlongwei/logserver/wikis/redis)
9. -Dfiles=false关闭files页签，-Dlogger=logserver@log配置logger页签，-Dmask=password(3,8)配置日志脱敏（password之后的第3至8个字符加星*）

#### [logserver-spring-boot-starter](https://gitee.com/xlongwei/logserver-spring-boot-starter)：使用[配置](https://gitee.com/xlongwei/logserver/wikis/regist)的方式更省事
```xml
<dependency>
    <groupId>com.xlongwei.logserver</groupId>
    <artifactId>logserver-spring-boot-starter</artifactId>
    <version>0.0.3</version>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

-Dlogserver.token=xlongwei
-Dlogserver.remoteHost=192.168.1.99
-Dmanagement.endpoints.web.exposure.include=logserver

logserver:
  remoteHost: 192.168.1.99 #指定logserver
  token: xlongwei #安全校验，需要与logserver的lajax.token一致
management: #需要依赖spring-boot-starter-actuator
  endpoints:
    web:
      exposure:
        include: logserver #开启LogserverEndpoint，让logserver变更日志级别
```

#### 前端日志

1. [lajax](https://github.com/eshengsky/lajax)：var logger = new Lajax(url); logger.info(arg1,...args);
2. [logserver.js](https://log.xlongwei.com/logserver.js)：Lajax.logLevel='info'; Lajax.logServer=false; Lajax.logConsole=true; Lajax.token='xlongwei';
3. [uni-app](https://gitee.com/xlongwei/apidemo/blob/master/common/logserver.js)：var logger = require('../../common/logserver.js').logger; logger.info('onReady index.vue');

```
var logger = new Lajax({
	url:'/lajax',//日志服务器的 URL
	autoLogError:false,//是否自动记录未捕获错误true
	autoLogRejection:false,//是否自动记录Promise错误true
	autoLogAjax:false,//是否自动记录 ajax 请求true
	//logAjaxFilter:function(ajaxUrl, ajaxMethod) {
	//	return false;//ajax 自动记录条件过滤函数true记录false不记录
	//},
	stylize:true,//是否要格式化 console 打印的内容true
	showDesc:false,//是否显示初始化描述信息true
	//customDesc:function(lastUnsend, reqId, idFromServer) {
	//	return 'lajax 前端日志模块加载完成。';
	//},
	interval: 5000,//日志发送到服务端的间隔时间10000毫秒
	maxErrorReq:3 //发送日志请求连续出错的最大次数
});
```

#### [log4j2日志](https://gitee.com/xlongwei/logserver/wikis/log4j2)
通常会再包一层AsyncAppender，并且还会有FileAppender写入日志文件，这里使用UDP协议的JSON格式日志，既不用增加端口也不用增加log4j依赖。

```
<Configuration status="debug">
	<Appenders>
		<Socket name="SOCKET" host="localHost" port="6000" protocol="UDP">
			<JSONLayout compact="true" properties="true" />
		</Socket>
	</Appenders>
	<Loggers>
		<Root level="all">
			<AppenderRef ref="SOCKET" />
		</Root>
	</Loggers>
</Configuration>
```

##### 整体设计

[架构设计](http://115.28.229.158/tool/images/logserver/logserver.png)：底层使用logback+socket、lajax+http传输日志，后端推荐logback.xml方式，可选starter依赖，前端支持web和uni-app形式，logserver可选使用light-search+lucene创建索引，详细用法见[wiki](https://gitee.com/xlongwei/logserver/wikis)。

[日志搜索](http://115.28.229.158/tool/images/logserver/search.png)


##### Nginx配置

登录认证见[pass.db](http://115.28.229.158/doku.php?id=tools:logstation)生成命令，nginx配置参考[log.conf](https://gitee.com/xlongwei/logserver/blob/master/aliyun/log.conf)。
