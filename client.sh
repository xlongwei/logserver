#!/bin/sh

logback=1.2.11 slf4j=1.7.32
if [ "$1" = "1.3" ]; then
logback=1.3.14 slf4j=2.0.7
elif [ "$1" = "1.4" ]; then
logback=1.4.14 slf4j=2.0.7
elif [ "$1" = "1.5" ]; then
logback=1.5.12 slf4j=2.0.15
fi
mvnrepos=/D/m2repos

cp=target/classes
cp=${cp}:${mvnrepos}/ch/qos/logback/logback-classic/${logback}/logback-classic-${logback}.jar
cp=${cp}:${mvnrepos}/ch/qos/logback/logback-core/${logback}/logback-core-${logback}.jar
cp=${cp}:${mvnrepos}/org/slf4j/slf4j-api/${slf4j}/slf4j-api-${slf4j}.jar
echo ${cp}

java -cp ${cp} -Dlogback.configurationFile=client.xml com.xlongwei.logserver.SocketClient2
